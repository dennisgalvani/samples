function createDataset(fields, constraints, sortFields) {
	
	var where = null;
	for (var c = 0; c < constraints.length; c++) {
		var field = constraints[c].fieldName;
		var value = constraints[c].initialValue;
		if(field == "nm_razao_emp" || field == "cd_emp"	|| field == "nr_cnpj_emp"){
			where = " AND " +  field + " LIKE '%" + value + "%' ";
		}
	}
	
	var table = getMetaList("ds_empresa");
	var query = "SELECT e.nm_razao_emp,e.cd_emp,e.nr_cnpj_emp, e.version FROM " + table 
		+ " e WHERE e.version = (SELECT MAX(e2.version) FROM " + table + " e2 "
		+ "WHERE e.documentid = e2.documentid) AND e.cd_emp IS NOT NULL "
		+ " AND (SELECT count(d.NR_DOCUMENTO) from documento d where d.COD_EMPRESA = 1 AND d.NR_DOCUMENTO = e.documentid AND d.VERSAO_ATIVA = 1) > 0 "
		if(where != null){
			query += where
		}
		+ "ORDER BY e.nm_razao_emp";
	return DatasetFactory.getDataset("ds_sql_consulta_fluig", new Array(query),
			null, null);
}

function getMetaList(dataset) {
	var constraints = new Array();
	constraints.push(DatasetFactory.createConstraint("company", "1", "1",
			ConstraintType.MUST));
	constraints.push(DatasetFactory.createConstraint("dataset", dataset,
			dataset, ConstraintType.MUST));
	return DatasetFactory.getDataset("ds_sql_get_metalist_formid", null,
			constraints, null).getValue(0, "metalist");
}